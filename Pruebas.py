from itertools import repeat
from typing import Dict

import numpy as np

# Dictionary price elasticity of the demand
elasticity = {
    "70": 40,
    "80": 34,
}


def calculate_friday(c: float, d: float, p: int) -> float:
    revenue = p * min(d, c)
    return revenue


def calculate_thursday(c1: int, d: float, p: int, c2: int, results: Dict) -> \
        float:
    revenue = (p * min(d, c1)) + results[c2][1]
    return revenue


# Friday Strategy
def dynp_hotel():
    k = 2
    results_f = {}
    for c2 in range(0, 3):
        r_max = 0
        p_max = 0
        for price in elasticity.keys():
            demand = np.maximum(
                np.random.poisson(lam=elasticity[price], size=k), 0)
            print(demand)
            print(np.mean(demand))
            print(np.std(demand))
            #sns.histplot(data=demand, binwidth=0.1, stat="probability")
            # plt.hist(demand, bins=30)
            #plt.show()
            revenue_friday = map(calculate_friday, repeat(c2, k), demand,
                                 repeat(int(price), k))
            e_profit_f = sum(revenue_friday) / k
            if e_profit_f > r_max:
                r_max = e_profit_f
                p_max = price
        results_f.update({c2: [p_max, round(r_max, 2)]})
    print(results_f)

    # Thursday Strategy
    results_f = {}
    for c2 in range(0, 3):
        r_max = 0
        p_max = 0
        for price in elasticity.keys():
            demand = np.maximum(
                np.random.poisson(lam=elasticity[price], size=k), 0)
            print(demand)
            print(np.mean(demand))
            print(np.std(demand))
            #sns.histplot(data=demand, binwidth=0.1, stat="probability")
            # plt.hist(demand, bins=30)
            #plt.show()
            revenue_friday = map(calculate_friday, repeat(c2, k), demand,
                                 repeat(int(price), k))
            e_profit_f = sum(revenue_friday) / k
            if e_profit_f > r_max:
                r_max = e_profit_f
                p_max = price
        results_f.update({c2: [p_max, round(r_max, 2)]})
    print(results_f)



if __name__ == '__main__':
    dynp_hotel()
