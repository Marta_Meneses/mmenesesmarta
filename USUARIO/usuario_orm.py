import sqlite3
from email_validator import validate_email, EmailNotValidError
import re

conn = sqlite3.connect('users_register.db')


class Field:
    """
    Descriptor tu manage attributes and actualizations in tha database.
    """

    def __set_name__(self, owner, name):
        """
        Called when the owning class owner is created. The descriptor has been
        assigned to name.
        :param owner: Class in which the descriptor has been created: User
        :param name: The name of the descriptor (Ex: age)
        """
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, obj, obj_type=None):
        """
        This attribute is called when you want to retrieve the information
        (value = obj. attr)
        :param obj: It is the instance or object of the class User
        :param obj_type: Class in which the descriptor has been created: User
        """
        return conn.execute(self.fetch, [obj.id]).fetchone()[0]

    def __set__(self, obj, value):
        """
        Called to set the attribute of an instance of the owner class to a new
        value
        :param obj: It is the instance or object of the class User
        :param value: Value which is being assigned to the attribute
        """
        conn.execute(self.store, [value, obj.id])
        conn.commit()


class EmailValidator(Field):
    """
    Validates the email.
    """

    def __set__(self, obj, value: str):
        """
        Called to set the value of the email attribute of an instance of the
        User class to a new value if the format is the required one, if not,
        it will raise a ValueError
        :param obj: Instance of the class User
        :param value: New value of the attribute email
        """
        try:
            valid = validate_email(value)
            value = valid.email
        except EmailNotValidError as e:
            print(str(e))
        else:
            conn.execute(self.store, [value, obj.id])
            conn.commit()


class AgeValidator(Field):
    """
    Validates the age which has to be a positive number no higher
    than 120
    """

    def __set__(self, obj, value: int):
        """
        Called to set the new value of the attribute age of the instance of the
        class User when the age is a positive value lower or equal to 120.
        :param obj: Instance of the class User
        :param value: New value of the age attribute
        """
        if 0 <= value <= 120:
            conn.execute(self.store, [value, obj.id])
            conn.commit()
        else:
            raise ValueError


class PhoneValidator(Field):
    """
    Validates the phone number so that the unique valid formats are
    a format of 9 numbers or of 10 or 11 numbers preceded of "+"
    """

    def __set__(self, obj, value: str):
        """
        Called to set the new value of the phone attribute ( valid for phone_1
        and for phone_2) for the instance of the class User if the format is the
        correct one.
        :param obj: Instance of the class User
        :param value: New value for the phone
        """
        if bool(re.fullmatch(r"[0-9]{9}", value)) or \
                bool(re.fullmatch(r"\+[0-9]{10,11}", value)):
            conn.execute(self.store, [value, obj.id])
            conn.commit()
        else:
            raise ValueError


class User(object):
    """
    Class for user which has "id" as attribute and the rest are defined
    through lookups and dynamic actualizations in the database using descriptors
    """
    table = 'users'  # Table name
    key = 'id'  # Primary key
    name = Field()
    surname = Field()
    email = EmailValidator()
    age = AgeValidator()
    phone_1 = PhoneValidator()
    phone_2 = PhoneValidator()

    def __init__(self, id_user: int):
        """
        :param id_user: Unique identifier of the user
        """
        self.id = id_user


# Instance of the class User
bob = User(1)

# Age assignment
try:
    bob.age = 20
except ValueError:
    print("Value Error: Inappropriate age value")
except TypeError:
    print("Type Error: The age must be an integer ")

try:
    assert bob.age == 100
except AssertionError:
    print("Assertion Error")

# Age updated
try:
    bob.age = 40
except ValueError:
    print("Value Error: Inappropriate age value")
except TypeError:
    print("Type Error: The age must be an integer ")

# We prove the assignment is done correctly
try:
    assert bob.age == 40
except AssertionError:
    print("Assertion Error")

# New age assignment
try:
    bob.age = 200
except ValueError:
    print("Value Error: Inappropriate age value")
except TypeError:
    print("Type Error: The age must be an integer ")

# Email Assignment
try:
    bob.email = "r.lucerga@uf"
except ValueError:
    print("Value Error: Inappropriate email value")
except TypeError:
    print("Type Error: The email must be a string ")

try:
    bob.email = "r.lucerga@ufv.es"
except ValueError:
    print("Value Error: Inappropriate email value")
except TypeError:
    print("Type Error: The email must be a string ")

# Phone assignment
try:
    bob.phone_1 = "4444444"
except ValueError:
    print("Value Error: Inappropriate phone value")
except TypeError:
    print("Type Error: The phone number must be a string ")

try:
    bob.phone_1 = "683562821"
except ValueError:
    print("Value Error: Inappropriate phone value")
except TypeError:
    print("Type Error: The phone number must be a string ")

try:
    bob.phone_1 = 683562821
except ValueError:
    print("Value Error: Inappropriate phone value")
except TypeError:
    print("Type Error: The phone number must be a string ")
