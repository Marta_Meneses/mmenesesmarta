from itertools import repeat
import json
import numpy as np
from typing import Dict, Tuple
from multiprocessing import Pool

# Dictionary price elasticity of the demand
elasticity = {

    70: 40,
    80: 34,
    90: 30,
    100: 27,
    110: 24,
    120: 22,
    140: 18,
    160: 15,
}


def calculate_friday(c: int, d: int, p: int) -> float:
    """
    Function that calculates the revenues for Friday given a price, a demand
    and the number of rooms available.
    :param c: Number of rooms left on Thursday
    :param d: Demand for Friday
    :param p: Price of the room for Friday
    :return: Revenue calculated
    """
    revenue = p * min(d, c)
    return revenue


def calculate_thursday(c1: int, d: int, p: int, results: Dict) -> \
        float:
    """
    Function that calculates the revenues for Thursday given a price, a demand,
    the number of rooms left on Wednesday and the expected revenues for Friday.
    :param c1: Rooms left on Wednesday.
    :param d: Demand for Thursday
    :param p: Price of the room for Thursday
    :param results: Dictionary that given a number of rooms left on Thursday,
    will give as the price of the room for Friday
    :return: Total revenues ( sum of thursday´s and friday´s revenues).
    """
    c2 = c1 - min(c1, d)
    revenue = (p * min(d, c1)) + results[c2][1]
    return revenue


def friday_strategy(c2: int) -> Tuple:
    """
    Function that calculates the strategy to decide the best price for Friday
    depending on the rooms left on Thursday
    :param c2: Rooms left on Thursday
    :return: The best price for Friday for c2 and the expected revenues
    """
    k = 50000
    r_max = 0
    p_max = 0
    for price in elasticity.keys():
        demand = np.maximum(np.random.poisson(lam=elasticity[price], size=k),
                            0)
        revenue_friday = map(calculate_friday, repeat(c2, k), demand,
                             repeat(price, k))
        e_profit_f = sum(revenue_friday) / k
        if e_profit_f > r_max:
            r_max = e_profit_f
            p_max = price
    return p_max, round(r_max, 0)


def thursday_strategy(c1: int, results: Dict) -> int:
    """
    Function that calculates de best price for Thursday depending on the the
    rooms left on Wednesday
    :param c1: Rooms left on Wednesday
    :param results: Dictionary with the resulting price and revenues for Friday
    depending on the rooms left on Thursday
    :return: The best price for Friday depending on c1
    """
    k = 50000
    r_max = 0
    p_max = 0

    for price in elasticity.keys():
        demand = np.maximum(
            np.random.poisson(lam=elasticity[price], size=k), 0)
        revenue_thursday = map(calculate_thursday, repeat(c1, k), demand,
                               repeat(price, k), repeat(results, k))
        e_profit_t = sum(revenue_thursday) / k
        if e_profit_t > r_max:
            r_max = e_profit_t
            p_max = price
    return p_max


def dynp_hotel() -> None:
    """
    Function that creates the strategy for Friday and for Thursday to decide
    the price of the rooms for Thursday and for Friday depending on the number
    of rooms left for each day.
    """
    k = 50000
    with Pool() as pool:
        r2 = pool.map(friday_strategy, range(0, 71))
        results_f = dict(zip(range(0, 71), r2))

    with open('friday.json', 'w') as f:
        json.dump(results_f, f, sort_keys=True, indent=4)

    with Pool() as pool:
        r1 = pool.starmap(thursday_strategy, zip(range(0, 71),
                                                 repeat(results_f, k)))
        results_t = dict(zip(range(0, 71), r1))

    with open('thursday.json', 'w') as f:
        json.dump(results_t, f, sort_keys=True, indent=4)


if __name__ == '__main__':
    dynp_hotel()
