from typing import Set


class StockPiece(object):
    """
    The StockPiece class denotes a single portion belonging to a map of a fabric sheet and is
    characterized by geometrical attributes.
     """

    # First of all we create the constructor of the class StockPiece, defining the attributes of the class.
    def __init__(self, id_stock_piece: int, height: float, length: float, quality_level: str, positionx: float,
                 positiony: float, id_father_stock_piece: int):
        """

        :param id_stock_piece: Identifier of the stock piece
        :param height: Height of the stock piece
        :param length: Length of the stock piece
        :param quality_level: Quality degree of the stock piece
        :param positionx: Coordinate X starting from the left-top corner
        :param positiony: Coordinate Y starting from the left-top corder
        :param id_father_stock_piece: Reference to its generating stock piece in the initial map
        """
        self.id_stock_piece = id_stock_piece
        self.height = height
        self.length = length
        self.quality_level = quality_level
        self.positionx = positionx
        self.positiony = positiony
        self.id_father_stock_piece = id_father_stock_piece

    def get_area(self) -> float:
        """
        Calculates the area of the stock piece
        :return: Returns the area calculated
        """
        return self.height * self.length


class Order:
    """
    The Order class represents a single customer order defined by a collection of attributes.

    """

    def __init__(self, id_order: str, customer: str, raw_material: str, min_quality_level: str, min_height: float,
                 max_height: float, min_quantity: float, max_quantity: float, min_cut_lenght: float,
                 max_cut_lenght: float):
        """

        :param id_order: Identifier of the requested order
        :param customer: The customer who submits the order
        :param raw_material: Required raw material of the order
        :param min_quality_level: Minimum accepted quality degree
        :param min_height: Minimum accepted height
        :param max_height: Minimum accepted height
        :param min_quantity: Minimum quantity requested
        :param max_quantity: Maximum quantity requested
        :param min_cut_lenght: Minimum cut length
        :param max_cut_lenght: Maximum cut length
        """
        self.id_order = id_order
        self.customer = customer
        self.raw_material = raw_material
        self.min_quality_level = min_quality_level
        self.min_height = min_height
        self.max_height = max_height
        self.min_quantity = min_quantity
        self.max_quantity = max_quantity
        self.min_cut_lenght = min_cut_lenght
        self.max_cut_lenght = max_cut_lenght

        self.stockpieces = {}

    def add_stockpiece_order(self, stockpiece: StockPiece) -> dict:
        """
        Adds a stock piece to an Order.
        :param stockpiece: The instance of StockPiece that we will add to an order
        :return: The dictionary updated with the new stock piece added
        """
        self.stockpieces[stockpiece.id_stock_piece] = stockpiece  # To access the stock piece added to the
        # dictionary we use a key which, in this case, is the identifier of the stock piece
        return self.stockpieces

    def get_assigned_quantity(self) -> float:
        """
        Calculates the area related to the stock pieces assigned. In this method we need to sum the area of the
        different stock pieces assigned to the Order. The area is calculated multiplying the height by the length of
        each stock piece :return: the area calculated
        """
        area = 0.0
        for sp in self.stockpieces.values():
            area += sp.get_area()
        return area


class FabricSheet:
    """
    FabricSheet class denotes a single fabric sheet with its physical, geometrical and logistical features.
    Since the evolution of the structure of each fabric sheet is represented by three maps whose type is specified by
    mapType parameter, which can assume Initial, Intermediate or Final values. One map may include multiple
    stock pieces, while one stock piece may belong to only one map. On its turn, a fabric sheet has at most
    three maps, while one map is referred to only one fabric sheet
    """

    def __init__(self, id_fabric_sheet: str, raw_material: str, height: float, length: float, department: str,
                 weaving_forecast: bool):
        """

        :param id_fabric_sheet: Identifier of the Fabric Sheet created that provides further information about the
        state of the sheet, which can be classified as finished, semi-finished or raw.
        :param raw_material: The raw material
        :param height: Total height of the Fabric Sheet
        :param length: Total length of the Fabric Sheet
        :param department: Concerns the location of the Fabric Sheet
        :param weaving_forecast: Tha nature of the fabric sheet is real (0)  or forecasted (1)
        """
        self.id_fabric_sheet = id_fabric_sheet
        self.raw_material = raw_material
        self.height = height
        self.length = length
        self.department = department
        self.weaving_forecast = weaving_forecast
        self.maps = {"initial": Map("final", True), "intermediate": Map("intermediate", True),
                     "final": Map("initial", True)}


class Map:
    """
    Is the detailed information about the stock-pieces that are contained in a reel or a sheet. Each map is composed
    by a set of stock pieces. Map class is the representation of the initial, intermediate or final structure(
    MapType) of a fabric sheet.
    """

    def __init__(self, Maptype: str, Real: bool):
        """

        :param Maptype: Indicates if it is the initial, intermediate ot final map
        :param Real: A Boolean attribute (Real) specifies if the map is real or referred to an expected
        realistic structure.
        """
        self.Maptype = Maptype
        self.Real = Real
        self.stockpieces = set([])

    def add_stockpiece(self, stockpiece: StockPiece) -> Set[StockPiece]:
        """
        Adds a stock piece to the map
        :param stockpiece: An instance of the class StockPiece we add to the map
        :return: The stock piece created to add to the map
        """
        self.stockpieces.add(stockpiece)
        return self.stockpieces
