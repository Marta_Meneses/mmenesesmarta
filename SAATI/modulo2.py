import SAATI.modulo1

# PRINCIPAL PROGRAM
first_order = SAATI.modulo1.Order("1", "John", "textile", "maximum", 4.0, 4.0, 4.0, 4.0, 4.0, 4.0)
first_fabric_sheet = SAATI.modulo1.FabricSheet("Finished", "textile", 8.0, 8.0, "manufacturing", True)
piece1 = SAATI.modulo1.StockPiece(1, 1.0, 1.0, "medium", 2.0, 2.0, 2)
piece2 = SAATI.modulo1.StockPiece(2, 1.0, 1.0, "medium", 2.0, 2.0, 2)
piece_map1 = first_fabric_sheet.maps["intermediate"].add_stockpiece(piece1)
stock_piece2 = first_fabric_sheet.maps["intermediate"].add_stockpiece(piece2)
piece1_order = first_order.add_stockpiece_order(piece1)
piece2_order = first_order.add_stockpiece_order(piece2)
a = first_order.get_assigned_quantity()
print(a)
